<html>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
    
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

</html>
<?php

    session_start();

    require_once './config/route.php';

    require_once './library/input.php';
    require_once './library/instance.php';

    require_once './library/route.php';

    require_once './library/database.php';
    require_once './library/calendar.php';

    require_once './helper/common.php';

    $instance = new instance();

    $instance->db = database::get();
    $instance->c = new draw_calendar(input::get('date'));

    $route = new route($config['route']);
    $route->exec(input::get('route'));
