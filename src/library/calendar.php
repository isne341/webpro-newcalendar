<html>
    
    <script>        

        function popup(id) {
            var popup = document.getElementById(id);
            popup.classList.toggle('show');
            popup.focus();
        }
        
        $(document).ready(function()
        {
            
                $("#show_login").click(function(){
                    showlogin();
            });
                $("#close_login").click(function(){
                    hidelogin();
            });
                $("#show_regis").click(function(){
                    showregis();
            });
                $("#close_regis").click(function(){
                    hideregis();
            });
                $("#show_add").click(function(){
                    showadd();
            });
                $("#close_add").click(function(){
                    hideadd();
            });
            
            $(function(){
                $( "h4" ).draggable();
                $( "li.date, li.event" ).droppable({
                  
                    drop: updateEvent 
                    
                    /*function( event, ui ) {
                    $( "h4#d5" )
                        .html( "Dropped!" );
                    }*/
                });
            });
            
            function updateEvent(event, ui){
                
                var draggable = ui.draggable;
                
                var id = draggable.attr('id').split("d", 1);
                
                var newDate = $(this).attr('date');
                
                
                $.ajax({
                    type:'POST',
                    url: 'http://localhost/calendar/src/library/edit_ajax.php',
                    data:{id:id[0], date:newDate},
                    success: function(){
                        location.reload();
                    }
                });
                
                
            }

            function showlogin()
            {
                $("#loginform").fadeIn();
                $("#loginform").css({"visibility":"visible","display":"block"});
            }

            function hidelogin()
            {
                $("#loginform").fadeOut();
                $("#loginform").css({"visibility":"hidden","display":"none"});
            }

            function showregis()
            {
                $("#regisform").fadeIn();
                $("#regisform").css({"visibility":"visible","display":"block"});
            }

            function hideregis()
            {
                $("#regisform").fadeOut();
                $("#regisform").css({"visibility":"hidden","display":"none"});
            }
            
            function showadd()
            {
                $("#addform").fadeIn();
                $("#addform").css({"visibility":"visible","display":"block"});
            }

            function hideadd()
            {
                $("#addform").fadeOut();
                $("#addform").css({"visibility":"hidden","display":"none"});
            }
            
        });
    
    </script>

</html>

<?php

class calendar extends instance
{

    private $event = array();
    private $select;
    protected $next;
    protected $prev;
    protected $calendar;

    public function __construct($select = false)
    {
        parent::__construct();
        $this->instance = get_instance();
        if ($select == false) {
            $this->select = date('Y-m');
        } else {
            $this->select = date('Y-m', strtotime($select));
        }

        $this->next = date('Y-m', (strtotime('next month', strtotime($this->select))));
        $this->prev = date('Y-m', (strtotime('previous month', strtotime($this->select))));
    }

    public function get_event($id)
    {
        $result = $this->instance->db->prepare('SELECT * FROM `appointments` where `id` = :id ');

        $result->bindParam(':id', $id);

        $result->execute();

        return $result->fetchall(PDO::FETCH_ASSOC);
    }
    
    public function del_event($id){
        
        $result = $this->instance->db->prepare('DELETE FROM `appointments` where `id` = :id ');

        $result->bindParam(':id', $id);

        $result->execute();
        
    }


    public function add_event($event)
    {
        $this->event[$event['date']] = $event;

        $result = $this->instance->db->prepare('INSERT INTO `appointments`(`date`, `title`, `description`, `owner`) VALUES (:date,:title,:description,:id)');

        $result->bindParam(':date', $event['date']);
        $result->bindParam(':title', $event['title']);
        $result->bindParam(':description', $event['description']);
        $result->bindParam(':id', $event['owner']);

        $result->execute();
    }
    
    
    public function edit_event($event)
    {

        $result = $this->instance->db->prepare('UPDATE `appointments` SET date=:date, title=:title, description=:description WHERE id=:post_id');

        $result->bindParam(':date', $event['date']);
        $result->bindParam(':title', $event['title']);
        $result->bindParam(':description', $event['description']);
        $result->bindParam(':post_id', $event['id']);

        $result->execute();
        
    }    

    private function load_event()
    {

        $req = $this->instance->db->prepare('SELECT * FROM `appointments` WHERE `owner`=:owner');
        
        $req->bindParam(':owner', $_SESSION['id']);
        
        $req->execute();
        
        $result = $req->fetchall(PDO::FETCH_ASSOC);

        foreach ($result as $event) {
            $this->event[date('Y-m-d', strtotime($event['date']))][] = $event;
        }

    }
    
    protected function genarate()
    {

        $this->load_event();

        $calendar_array = array();

        $date = date('Y-m-d', strtotime($this->select));

        $number_day = date('t', strtotime($date));
        
        $day = date('d', strtotime($date));
        
        $month = date('m', strtotime($date));

        $date_select = date('Y-m', strtotime($this->select));

        if (date('Y-m', strtotime($date)) != $date_select) {
            continue;
        }

        
        $year = date('Y', strtotime($date));

        $this->calendar['month'] = date('F', strtotime($date));
        $this->calendar['year']  = $year;

        $week = 0;

        for ($i = 1; $i <= $number_day; $i++) {
            
            $tmp      = mktime(0, 0, 0, $month, $i, $year);
            $tmp_date = date('Y-m-d', $tmp);
            $dow      = date('w', $tmp);
            if ($dow == 0) {
                $week++;
            }
            $calendar_array[$week][$dow]['day']  = date('d', $tmp);
            $calendar_array[$week][$dow]['date'] = date('Y-m-d', $tmp);

            if (isset($this->event[$tmp_date])) {
                
                $calendar_array[$week][$dow]['event'] = $this->event[$tmp_date];
            }

            if ($tmp_date == date('Y-m-d')) {
                $calendar_array[$week][$dow]['today'] = true;
            }

        }

        
        return $calendar_array;
    }

}

class draw_calendar extends calendar
{

    public function draw()
    {

        $now_d = date('d');
        $now_m = date('F');

        $calendar = $this->genarate();
        
        if(!isset($_SESSION['user'])){
            //month year , guest panel
            $html = '<div class="month"> 

                        <ul>     

                            <li style="margin-left:100px;">Please

                                [<a id="show_regis" style="color:#fff300 !important;cursor:pointer">Register</a>]
                                or
                                
                                    <div id="regisform">
                                      
                                      <form id="from" action="./index.php?route=r_process" method="post">
                                         <h2>Register!</h2>
                                         
                                          <input type = "button" id = "close_regis" value="close">
                                          <input name="username" type="text" placeholder="Username" id = "login">
                                          <input name="password" type="password" placeholder="Password" id = "password">
                                          <input name="c-password" type="password" placeholder="Confirm Password" id = "c-password">

                                          <input type="submit" id = "doregis" value="submit" >
                                      </form>
                                      
                                    </div>
                                    
                                [<a id="show_login" style="color:#fff300 !important;cursor:pointer">Login</a>]
                                
                                    <div id="loginform">
                                      
                                      <form id="from" action="./index.php?route=l_process" method="post">
                                         <h2>Login!</h2>
                                         
                                          <input type = "button" id = "close_login" value="close">
                                          <input name="username" type="text" placeholder="Username" id = "login">
                                          <input name="password" type="password" placeholder="Password" id = "password">

                                          <input type="submit" id = "dologin" value="submit">
                                      </form>
                                      
                                    </div>
                                
                                <br> 

                                <span style="font-size:18px; margin-left: 50px">Before using our calendar.</span> 

                            </li> 

                        </ul> 

                    </div>';
        }
        else{
            //user panel
            $html = '<div class="month"> 

                        <ul> 

                            <li class="prev">

                                <a href="./index.php?route=index&date=' . $this->prev . '">&#10094;</a>

                            </li>

                            <li class="next">
                            
                                <a href="./index.php?route=index&date=' . $this->next . '">&#10095;</a>

                            </li>

                            <li style="margin-left:50px;display: inline-block">
                                
                                <span style="font-size:24px">' . $this->calendar['year'] . '</span><br>
                                
                                <span style="margin-left: 35px">' . $this->calendar['month'] . '</span>

                                [<a id="show_add" style="color:#fff300 !important;cursor:pointer">New Event</a>]
                                
                                    <div id="addform">
                                      
                                      <form id="from" action="./index.php?route=process" method="post">
                                         <h2>Add your appointment</h2>
                                          
                                          <input type = "button" id = "close_add" value="close">
                                          
                                         <input name="title" id = title type="text" placeholder="Title" >

                                         <input name="date"  id = "date" type="date" value="<?php echo input::get("date"); ?> 

                                         <textarea name="description" id="description" placeholder="Describe here..."></textarea>

                                          <input type="submit" id = "doadd" value="submit">
                                      </form>
                                      
                                    </div>



                            </li>
                            
                            <li style="float: right">
                                
                                <span style="font-size: 24px;margin-right: 50px; float:right">Welcome</span><br>
                                
                                <span style="float:right;margin-right:40px">[<a href="./index.php?route=logout" style="color:#fff300 !important;">Logout</a>]</span>
                                <span style="font-size:20px; float:right;margin-right: 5px">' . $_SESSION['user'] . '</span> 
                                
                            </li>

                        </ul> 

                    </div>';
        }

        //calendar header
        $html .= '<ul class="weekdays"> <li>SUN</li> <li>MON</li> <li>TUE</li> <li>WED</li> <li>THU</li> <li>FRI</li> <li>SAT</li> </ul>';

        $html .= '<ul class="days">';
        foreach ($calendar as $week) {
            for ($dow = 0; $dow < 7; $dow++) {
                
                if (isset($week[$dow]['event'])) {
                    
                    $html .= $this->event_element($week[$dow]);
                    
                } else if (isset($week[$dow]['day'])) {
                    
                    if (isset($week[$dow]['today'])) {
                        
                        $html .= $this->active_element($week[$dow]);
                        
                    } else {
                        
                        $html .= $this->date_element($week[$dow]);
                        
                    }
                    
                } else {
                    
                    $html .= $this->blank_element();
                    
                }
                
            }
            
            $html .= '<br>';
            
        }
        
        $html .= '</ul>';

        return $html;
    }

    private function blank_element()
    {
        return '<li class="blank"></li>';
    }

    private function date_element($day)
    {
        return '<li class="date" date="'. $day['date'] .'"><a href="./index.php?route=add&date=' . $day['date'] . '" title="today">' . $day['day'] . '</a></li>';
    }

    private function active_element($day)
    {
        return '<li class="active" date="'. $day['date'] .'">
                    <a href="./index.php?route=add&date=' . $day['date'] . '" title="today">' . $day['day'] . '</a>
                </li>';
    }

    private function event_element($day)
    {
        $html = '<li class="event" date="'. $day['date'] .'">';
            $html.= '<a href="./index.php?route=add&date=' . $day['date'] . '" >';
                $html.= $day['day'];
            $html.='</a>';
            $html.='<div class="list">';
            foreach ($day['event'] as $event) {
                    
                        $html.= '<h4 id="'.$event['id'].'d" ondblclick="popup('.$event['id'].')">

                                    ' . $event['title'] . '

                                    <span class="popuptext" id="'.$event['id'].'">
                                        Title: '.$event['title'].'<br><br>
                                        Date: '.date("l jS \of F Y", strtotime($event['date'])).'<br><br>
                                        Details: '.$event['description'].'
                                        <br><br>
                                        
                                        <a class="panel" href="./index.php?route=edit&id=' . $event['id'] .'&title=' . $event['title'] .'&date=' . $event['date'] .'&description=' . $event['description'] .'" >Edit </a>
                                        
                                        
                                        <span style="font-size: 20px">||</span>
                                        <a class="panel" href="./index.php?route=delete&id=' . $event['id'] .'"> Delete</a>

                                    </span>

                                </h4>';
                        $html.='</a>';
                    
            }
            $html.='</div>';
            $html.='</li>';

        return $html;
    }
}





