<?php
class database 
{
    private static $db;

    public function get(){
        if(!isset(self::$db)){
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            self::$db = new PDO('mysql:host=localhost;dbname=calendar', 'root', '', $pdo_options);
            
        }
        return self::$db;
    }

}
