<?php

    class login extends instance{
        
        public function index(){
            require_once('./views/layout/header.php');
            require_once('./views/l_form.php');
        }

        public function process(){
            
		    $this->instance = get_instance();
            
            if($req = $this->instance->db->prepare('SELECT * FROM `user` WHERE `username` = :user LIMIT 1')){
        
                $req->bindParam(':user', input::post('username'));
                
                $req->execute();

                $result = $req->fetch();

                if($req->rowCount() == 1){

                    if(input::post('password') == $result['password']){

                        $_SESSION['user'] = $result['username'];
                        $_SESSION['id'] = $result['id'];
                    }
                }
            }

            header('location:./index.php?route=index');
        }
        
        public function logout(){
            
            session_destroy();
            
            header('location:./index.php?route=index');

        }

    }