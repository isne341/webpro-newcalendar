<?php

    class register extends instance{
        
        public function index(){
            require_once('./views/layout/header.php');
            require_once('./views/r_form.php');
        }

        public function process(){
            
            if(input::post('c-password') == input::post('password')){
                
                $this->instance = get_instance();

                $result = $this->instance->db->prepare('INSERT INTO `user`(`username`, `password`) VALUES (:username,:password)');

                $result->bindParam(':username', input::post('username'));
                $result->bindParam(':password', input::post('password'));

                $result->execute();
            }
            
            header('location:./index.php?route=index');
        }

    }

?>