<?php

    class edit{

        public function index(){
            require_once('./views/layout/header.php');
            require_once('./views/e_form.php');
        }

        public function process(){
            
		  $instance = get_instance();

            $event = array(
                'id'          => input::post('id'),
                'date'        => date('Y-m-d',strtotime(input::post('date'))),
                'title'       => input::post('title'),
                'description' => input::post('description'),
            );
		    $instance->c->edit_event($event);

            header('location:./index.php?route=index');
        }

    }













