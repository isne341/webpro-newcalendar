<?php

    class dashboard{

        public function __construct(){
            require_once('./views/layout/header.php');
        }

        public function index(){

            $instance = get_instance();

            $c = $instance->c->draw();

            require_once('./views/calendar.php');

        }

        public function show(){
            
            $instance = get_instance();

            $event_array = $instance->c->get_event(input::get('id'));

            require_once('./views/show.php');
            
        }

    }