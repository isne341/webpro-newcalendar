<?php

    class add{

        public function index(){
            require_once('./views/layout/header.php');
            require_once('./views/form.php');
        }

        public function process(){
            
		  $instance = get_instance();

            $event = array(
                'date'        => date('Y-m-d',strtotime(input::post('date'))),
                'title'       => input::post('title'),
                'description' => input::post('description'),
                'owner'       => $_SESSION['id'],
            );
            
		    $instance->c->add_event($event);

            header('location:./index.php?route=index');
        }

    }