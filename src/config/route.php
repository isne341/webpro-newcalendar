<?php
$config['route']['index'] = 'dashboard/index'; // show calendar
//$config['route']['show'] = 'dashboard/show'; // show appointment


$config['route']['add'] = 'add/index'; // show form
$config['route']['process'] = 'add/process'; // process when you insert calendar to database


$config['route']['edit'] = 'edit/index'; // edit form
$config['route']['e_process'] = 'edit/process'; // process when you edit calendar to database
$config['route']['e_ajax'] = 'edit/ajax'; // process when you edit calendar to database

$config['route']['delete'] = 'delete/process'; // delete appointment

$config['route']['login'] = 'login/index'; 
$config['route']['l_process'] = 'login/process'; 
$config['route']['logout'] = 'login/logout'; 

$config['route']['register'] = 'register/index'; 
$config['route']['r_process'] = 'register/process'; 

$config['route']['404'] = 'dashboard/index'; // file not found
